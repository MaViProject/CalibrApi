// common utility functions. To change in generics
package utils

import (
	"fmt"
	"os"
	"strings"
)

/*** Operations on structures ***/

// StringInSlice gets a string and a slice and returns true if the string value is in the slice, false otherwise. Converts everything in lower-case to avoid mismatches.
func StringInSlice(toSearch string, list []string) bool {
	//converts item toSearch in lower-case
	toSearch = strings.ToLower(toSearch)
	for _, item := range list {
		if strings.ToLower(item) == toSearch {
			return true
		}
	}
	return false
}

// ContainedSlice checks if all the elements in toSearch array are inside list array, if so, returns ture
func ContainedSlice(toSearch []string, list []string) (contained bool) {
	for _, ts := range toSearch {
		if StringInSlice(ts, list) {
			contained = true
		} else {
			contained = false
		}
	}
	return contained
}

// SameContentSlice checks if the arrays of strings are equals (same content independent by the order) then returns true.
func SameContentSlice(sliceA []string, sliceB []string) bool {
	// the slices must have the same length to be equal
	if len(sliceA) != len(sliceB) {
		return false
	}
	// if the length is the same, check all the elements
	for _, sa := range sliceA {
		contained := false
		for _, sb := range sliceB {
			if sa == sb {
				contained = contained || true
			} else {
				contained = contained || false
			}
		}
		// if at least 1 element of the first slice is not contained in the second then they're not equal
		if !contained {
			return false
		}
	}
	// if the previous condition is never encountered then the content is the same
	return true
}

// MapVal gets the list of values from the map
func MapVal(attrs map[string]string) (values []string) {
	for _, val := range attrs {
		values = append(values, val)
	}
	return values
}

// ReplaceChars gets a string and a map containing the character set to replace (key) with their replacement (values)
func ReplaceChars(text string, chars map[string]string) (replaced string) {
	for key, val := range chars {
		replaced = strings.Replace(text, key, val, -1)
	}
	return replaced
}

// Returns if the string matches with the criteria (starts, ends, contains, exact)
func CheckStringMatch(toCompare string, reference string, filter string) bool {
	tcLen := len(toCompare)
	refLen := len(reference)
	// in any case the string to compare cannot be longer than the reference
	if tcLen > refLen {
		return false
	}
	// check possible the cases
	switch filter {
	case "starts":
		if toCompare == string(reference[0:tcLen]) {
			return true
		}
		return false
	case "ends":
		if toCompare == string(reference[refLen-tcLen:refLen]) {
			return true
		}
		return false
	case "exact":
		if toCompare == reference {
			return true
		}
		return false
	case "contains":
		return strings.Contains(reference, toCompare)
	default:
		return false
	}
}

/*** File Operations ***/

// CreateFolder creates a folder basing on the path passed in `folder` string
func CreateFolder(folder string) (err error) {
	_, err = os.Stat(folder)
	if err != nil {
		//create folder if not exists
		if os.IsNotExist(err) {
			err = os.Mkdir(folder, os.ModePerm)
			if err != nil {
				err = fmt.Errorf("CreateFolder - Error while creating %s: %v", folder, err)
				return err
			}
		}
	}
	return err
}
