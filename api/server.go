package api

import (
	db "calibrapi/db"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

// Server serves HTTP requests for our * service (optionally with the store interacts with DB while processing requests from client)
type Server struct {
	store     *db.SQLiteCalWeb
	router    *gin.Engine
	basePath  string
	titlePlc  string
	dbAppPath string
}

// Create a server instance and setup the API routes for the service
func NewServer(store *db.SQLiteCalWeb, basePath string, titlePlc string, dbAppPath string) *Server {
	// create the server already adding the DB interaction
	server := &Server{
		store:     store,
		basePath:  basePath,
		titlePlc:  titlePlc,
		dbAppPath: dbAppPath,
	}
	server.setRouter()
	return server
}

func (server *Server) setRouter() {
	// create the router to add to the server
	router := gin.Default()
	// get gin validator which is an interface to convert to a validator pointer
	val, ok := binding.Validator.Engine().(*validator.Validate)
	if ok {
		// register the validation functions with their tags to use in the struct with bindings
		val.RegisterValidation("txtFilterCriteria", txtFilterCriteria)
		val.RegisterValidation("listFilterCriteria", listFilterCriteria)
	}

	// add the routes in groups for the router
	bk := router.Group("/books")
	{
		// add the routes for the books group
		//bk.Use(AuthorizeJWT("FE")) // middleware for all books group ops
		bk.GET("/dump", server.getLibrary)
		bk.GET("/dump/", server.getLibrary)
		bk.GET("/book", server.getBook)
		bk.GET("/book/", server.getBook)
		bk.POST("/list", server.getBooks)
		bk.POST("/list/", server.getBooks)
		bk.POST("/update", server.updBooks)
		bk.POST("/update/", server.updBooks)
	}

	// to manage all non-managed routes in any group
	router.NoRoute(func(c *gin.Context) {
		c.AbortWithStatusJSON(http.StatusNotFound, errorResponse(fmt.Errorf("Request has been sent for a non-existing route"), "noResourceErr"))
		return
	})
	// add the router to the server
	server.router = router
}

// Start runs the HTTP server on a specific address
func (server *Server) Start(address string) error {
	// start
	err := server.router.Run(address)
	// graceful shutdown
	//...
	return err
}
