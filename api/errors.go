// deals with error messages to return via API instead of using errors that may be use to gather information
package api

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// mapping between error code and message to return
var errMap = map[string]string{
	"loginErr":      "Wrong username/password",
	"formatErr":     "Request message has not been properly set",
	"srvErr":        "An error occurred while processing your request",
	"notAllowedErr": "You're not allowed to perform this operation",
	"noResourceErr": "Resource not found",
}

// errorResponse takes an error as input logs it and returns the message for the response as a gin.H object (map to store any key-value).
func errorResponse(err error, errmsg string) gin.H {
	//log the real error
	fmt.Println(fmt.Errorf(`errorResponse: %v, sending back "%s"`, err, errMap[errmsg]))
	return gin.H{"error": errMap[errmsg]}
}
