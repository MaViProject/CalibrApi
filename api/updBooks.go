// handler to manage the update requests

package api

import (
	"calibrapi/db"
	"net/http"

	"github.com/gin-gonic/gin"
)

// struct to get filters and book to updae data in a single json, composed by in QFilter and Book afterwards
type UpdRequest struct {
	Filter   db.QFilter     `json:"filter"`
	Update   db.Book        `json:"update"`
	ParseUpd db.ParseUpdate `json:"pupdate,omitempty"`
}

// updBooks calls the DB UpdateBooks method to update the books matching the criteria filter in the body using the update field in the body
func (server *Server) updBooks(ctx *gin.Context) {
	// map the data in the body structure for the request made by filter + book data to update
	var request UpdRequest
	err := ctx.ShouldBindJSON(&request)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err, "formatErr"))
		return
	}

	library, err := server.store.UpdateBooks(ctx, &request.Filter, &request.ParseUpd, &request.Update, server.basePath, server.titlePlc, server.dbAppPath)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err, "srvErr"))
		return
	}
	// if no reply from the DB (no book updated), return an empty array instead of nil
	if *library == nil {
		empty := []string{}
		ctx.JSON(http.StatusOK, empty)
		return
	}
	ctx.JSON(http.StatusOK, library)
	return
}

// updBooksByIds updates the book with the passed ids with the same info passed in the body

// updBooksByIdRange updates the book whose id is in the passed range with the same info passed in the body
