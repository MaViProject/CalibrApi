// handler to manage the select requests
package api

import (
	"calibrapi/db"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// getLibrary calls the DB GetLibrary method to get all the books in the library.
// to implement the pagination
func (server *Server) getLibrary(ctx *gin.Context) {
	library, err := server.store.GetLibrary()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err, "srvErr"))
		return
	}
	ctx.JSON(http.StatusOK, library)
	return
}

// getBook calls the DB GetLibrary method filtering by bookId through the GetBook interface.
func (server *Server) getBook(ctx *gin.Context) {
	// get the bookId as query parameter
	var filter db.QFilter
	err := ctx.ShouldBind(&filter)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err, "formatErr"))
		return
	}
	fmt.Println("FilterID:", filter.Id)
	library, err := server.store.GetBook(filter.Id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err, "srvErr"))
		return
	}
	ctx.JSON(http.StatusOK, library)
	return
}

// getBooks calls the DB GetBooks method to get the books matching the criteria in the body
// to implement the pagination
func (server *Server) getBooks(ctx *gin.Context) {
	// map the data in the body structure needed by GetBooks
	var filters db.QFilter
	err := ctx.ShouldBindJSON(&filters)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err, "formatErr"))
		return
	}

	library, err := server.store.GetBooks(&filters)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err, "srvErr"))
		return
	}
	// if no reply from the DB, return an empty array instead of nil
	if *library == nil {
		empty := []string{}
		ctx.JSON(http.StatusOK, empty)
		return
	}
	ctx.JSON(http.StatusOK, library)
	return
}
