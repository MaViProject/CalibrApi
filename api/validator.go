package api

import (
	"calibrapi/utils"

	"github.com/go-playground/validator/v10"
)

var (
	validTxtFilterCriteria  = []string{"contains", "exact", "starts", "ends"}
	validListFilterCriteria = []string{"contains", "exact"}
)

var txtFilterCriteria validator.Func = func(fl validator.FieldLevel) bool {
	// get the value of the field as interface converted to string
	criteria, ok := fl.Field().Interface().(string)
	if ok {
		// check if the value is supported or not
		return utils.StringInSlice(criteria, validTxtFilterCriteria)
	}
	// otherwise we don't get a string and return false
	return false
}

var listFilterCriteria validator.Func = func(fl validator.FieldLevel) bool {
	// get the value of the field as interface converted to string
	criteria, ok := fl.Field().Interface().(string)
	if ok {
		// check if the value is supported or not
		return utils.StringInSlice(criteria, validListFilterCriteria)
	}
	// otherwise we don't get a string and return false
	return false
}
