/* DB helpers */

package db

import (
	utils "calibrapi/utils"
	"database/sql"
	"fmt"
	"os"
	"strings"
)

/*** Additional SQL Functions ***/

// getBookAttrs only works for 1 or more instances of the same attribute. It returns the array of attributes related to the passed query for the book identified by the passed id
func (r *SQLiteCalWeb) getBookAttrs(qry string, id int64) (attrs []Attribute, err error) {
	rows, err := r.db.Query(qry, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	//iterates through the matching instances and append them in the array to return
	for rows.Next() {
		var attr Attribute
		if err := rows.Scan(&attr.key, &attr.val); err != nil {
			return nil, err
		}
		attrs = append(attrs, attr)
	}
	return
}

/*
// getBookAttrs only works for 1 or more instances of the same attribute. It returns the array of attributes related to the passed query for the book identified by the passed id
func (r *SQLiteCalWeb) getBookAttrs(qry string, id int64) (attrs []string, err error) {
	rows, err := r.db.Query(qry, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	//iterates through the matching instances and append them in the array to return
	for rows.Next() {
		var attr string
		if err := rows.Scan(&attr); err != nil {
			return nil, err
		}
		attrs = append(attrs, attr)
	}
	return
}
*/
// inCatalog gets checks if a value exists in a catalogue. If exists return the id.
func (r *Tx) inCatalog(value string, column string, table string) (id int64, err error) {
	qry := fmt.Sprintf("SELECT id FROM %s WHERE %s = ?", table, column)
	rows, err := r.tx.QueryContext(r.ctx, qry, value)
	if err != nil {
		err = fmt.Errorf("inCatalog - Error while querying the catalog %s looking for %s=%s: %v", table, column, value, err)
		return 0, err
	}
	defer rows.Close()

	//iterates through the matching instances and append them in the array to return
	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			err = fmt.Errorf("inCatalog - Error scanning query results for catalog %s looking for %s: %v", table, value, err)
			return 0, err
		}
	}
	return id, nil
}

// linked checks if the item is linked returning the catalog ids
func (r *SQLiteCalWeb) linked(table string, itemId int64, itemCol string, catCol string) (linkIds []int64, err error) {
	qry := fmt.Sprintf("SELECT %s FROM %s WHERE %s = ?", catCol, table, itemCol)
	rows, err := r.db.Query(qry, itemId)
	if err != nil {
		err = fmt.Errorf("linked - Error while querying the link table %s looking for %s %d and its link with %s: %v", table, itemCol, itemId, catCol, err)
		return nil, err
	}
	defer rows.Close()

	//iterates through the matching instances and return the result
	for rows.Next() {
		var linkId int64
		if err := rows.Scan(&linkId); err != nil {
			err = fmt.Errorf("inCatalog - Error scanning query results the link table %s looking for %s %d and its link with %s: %v", table, itemCol, itemId, catCol, err)
			return nil, err
		}
		linkIds = append(linkIds, linkId)
	}
	return linkIds, nil

}

// toCatalog adds the item to the catalog basing on the Id. Catalogues have trigger so deal with it.
func (r *Tx) toCatalog(value string, table string, col string, trigger string) (id int64, err error) {
	triggerQry := ""
	if trigger != "" {
		// get the sql statement to create the trigger
		triggerQry, err = r.getTrigger(trigger)
		if err != nil {
			err = fmt.Errorf("toCatalog - Error adding %s to catalog %s: %v", value, table, err)
			return 0, err
		}
		// drop the existing trigger as it creates problems with the update
		err = r.dropTrigger(trigger)
		if err != nil {
			err = fmt.Errorf("toCatalog - Error adding %s to catalog %s: %v", value, table, err)
			return 0, err
		}
	}
	//special case for the app.db
	var qry string
	if table == "shelf" {
		qry = fmt.Sprintf("INSERT INTO %s (%s, is_public, user_id) VALUES (?, 1, 1)", table, col)
	} else {
		qry = fmt.Sprintf("INSERT INTO %s (%s) VALUES (?)", table, col)
	}
	res, err := r.tx.ExecContext(r.ctx, qry, value)
	if err != nil {
		err = fmt.Errorf("toCatalog - Error while entering %s in catalog %s: %v", value, table, err)
		return 0, err
	}
	if trigger != "" {
		// re-create books_update_trg trigger
		err = r.createTrigger(triggerQry)
		if err != nil {
			err = fmt.Errorf("toCatalog - Error while entering %s in catalog %s: %v", value, table, err)
			return 0, err
		}
	}

	// get the ID of the insert
	id, err = res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

// toLink adds the couple item-catalog IDs to link them and returns the table entry id and the error
func (r *Tx) toLink(table string, itemId int64, itemCol string, catId int64, catCol string) (id int64, err error) {
	qry := fmt.Sprintf("INSERT INTO %s (%s, %s) VALUES (?, ?)", table, itemCol, catCol)
	res, err := r.tx.ExecContext(r.ctx, qry, itemId, catId)
	if err != nil {
		err = fmt.Errorf("toLink - Error while linking the %s %d to %s %d in %s: %v", itemCol, itemId, catCol, catId, table, err)
		return 0, err
	}
	id, err = res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

// updLink updates the couple item-catalog IDs instead of entering a new instance
func (r *Tx) updLink(table string, itemCol string, itemId int64, catCol string, exId int64, catId int64) (bool, error) {
	// updates basing on the item id and the previous cat id (to be updated)
	qry := fmt.Sprintf("UPDATE %s SET %s = ? WHERE %s = ? AND %s = ?", table, catCol, itemCol, catCol)
	res, err := r.tx.ExecContext(r.ctx, qry, catId, itemId, exId)
	if err != nil {
		err = fmt.Errorf("updLink - Error while updating the %s link for the item %s %d with catalog %s from value %d to %d: %v", table, itemCol, itemId, catCol, exId, catId, err)
		return false, err
	}

	err = checkResNo(res, "=", 1)
	if err != nil {
		err = fmt.Errorf("updLink - Error while updating the %s link for the item %s %d with catalog %s from value %d to %d: %v", table, itemCol, itemId, catCol, exId, catId, err)
		return false, err
	}
	return true, nil
}

// updAttributeList gets a peculiar parameter list (merge with the next one)
func (tx *Tx) updAttributeList(bookId int64, attrs *AttrListParams, r *SQLiteCalWeb) (err error) {
	// get the existing attributes
	attribs, err := r.getBookAttrs(attrs.Qry, bookId)
	if err != nil {
		err = fmt.Errorf("updListAttrs - error while getting bookID %d: %v", bookId, err)
		return err
	}
	// updates the attribute values basing on the retrieved values and the passed ones
	err = tx.updAttrList(bookId, attrs.BookCol, attribs, attrs.AttrList, attrs.LnkTbl, attrs.LnkCol, attrs.CatTbl, attrs.CatCol, attrs.TriggerName)
	if err != nil {
		err = fmt.Errorf("updListAttrs - error while updating bookID %d: %v", bookId, err)
		return err
	}
	return nil
}

// updAttrList updates the attribute by adding new items to catalogue and add/remove the links with the book to realize what's in the toSet from json
func (r *Tx) updAttrList(bookId int64, bookCol string, lattrs []Attribute, attrsConf []string, linkTab string, linkCol string, catTab string, catCol string, catTrigger string) (err error) {
	// arrays to store the ID of the attribute to remove and the names of the attribute to add (id is entered by DB)
	var toRemove []int64
	var toAdd []string

	// if attribute linked to book is in the config attributes array stays otherwise is removed
	for _, lattr := range lattrs {
		if !utils.StringInSlice(lattr.val, attrsConf) {
			toRemove = append(toRemove, lattr.key)
		}
	}

	// if attribute in config is not in the attributes linked to book list, then it must be added
	for _, attrConf := range attrsConf {
		if !utils.StringInSlice(attrConf, valAttrs(lattrs)) {
			toAdd = append(toAdd, attrConf)
		}
	}

	// remove the attributes by the book
	for _, attrId := range toRemove {
		qry := fmt.Sprintf("DELETE FROM %s WHERE %s = ? AND %s = ?", linkTab, bookCol, linkCol)
		_, err = r.tx.ExecContext(r.ctx, qry, bookId, attrId)
		if err != nil {
			err = fmt.Errorf("updAttrs - Error while removing attribute %s from book %d %v", catTab, bookId, err)
			return err
		}
	}

	// check if attributes to add are still to create
	for _, attrToAdd := range toAdd {
		attrId, err := r.inCatalog(attrToAdd, catCol, catTab)
		if err != nil {
			err = fmt.Errorf("updAttrs - Error while checking if attribute %s already exists in %s %v", attrToAdd, catTab, err)
			return err
		}
		// if is not in the catalog then add it
		if attrId == 0 {
			attrId, err = r.toCatalog(attrToAdd, catTab, catCol, catTrigger)
			if err != nil {
				err = fmt.Errorf("updAttrs - Error while adding the attribute %s to %s %v", attrToAdd, catTab, err)
				return err
			}
		}
		// link the attribute to book using the attrId just obtained
		lnkId, err := r.toLink(linkTab, bookId, bookCol, attrId, linkCol)
		if err != nil {
			err = fmt.Errorf("updAttrs - Error while linking the attribute %s to the book %d in table %s %v", attrToAdd, bookId, linkTab, err)
			return err
		}
		if lnkId == 0 {
			err = fmt.Errorf("updAttrs - Error while linking the attribute %s to the book %d in table %s: id in link table not generated", attrToAdd, bookId, linkTab)
			return err
		}
	}
	return nil
}

/*** TRIGGER MANAGEMENT ***/

// getTrigger retrieves the SQL statement to re-create the trigger
func (r *Tx) getTrigger(trigger string) (trgStatement string, err error) {
	rows, err := r.tx.QueryContext(r.ctx, "SELECT sql FROM sqlite_master where type = 'trigger' and name = ?", trigger)
	if err != nil {
		err = fmt.Errorf("getTrigger - Error while querying the master table to get %s trigger: %v", trigger, err)
		return "", err
	}
	defer rows.Close()

	//iterates through the matching instances and append them in the array to return
	for rows.Next() {
		if err := rows.Scan(&trgStatement); err != nil {
			err = fmt.Errorf("getTrigger - Error scanning query results for trigger %s: %v", trigger, err)
			return "", err
		}
	}
	return trgStatement, nil
}

// dropTrigger deletes the trigger passed as parameter
func (r *Tx) dropTrigger(trigger string) error {
	dropTriggerQry := fmt.Sprintf(`DROP TRIGGER "main"."%s"`, trigger)
	//res, err := r.db.Exec(dropTriggerQry, trigger)
	_, err := r.tx.ExecContext(r.ctx, dropTriggerQry)
	if err != nil {
		err = fmt.Errorf("dropTrigger - Error while dropping the trigger %s: %v", trigger, err)
		return err
	}
	/*
		err = checkResNo(res, "==", 1)
		if err != nil {
			err = fmt.Errorf("dropTrigger - Error while dropping the trigger %s: %v", trigger, err)
			return err
		}
	*/
	return nil
}

// createTrigger (re-)creates books_update_trg trigger
func (r *Tx) createTrigger(trigger string) error {
	_, err := r.tx.ExecContext(r.ctx, trigger)
	if err != nil {
		err = fmt.Errorf("createTrigger - Error while recreating trigger %s: %v", trigger, err)
		return err
	}
	/*
		err = checkResNo(res, "==", 1)
		if err != nil {
			err = fmt.Errorf("createTrigger - Error while recreating the trigger %s: %v", trigger, err)
			return err
		}
	*/
	return nil
}

/*** DB-UTILITY ***/

func checkResNo(res sql.Result, compare string, expected int64) (err error) {
	// checks the number of affected rows
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		err = fmt.Errorf("checkResNo - Error while retrieving the number of rows affected by the operation: %v", err)
		return err
	}
	bexpression := true
	switch compare {
	case "==":
		bexpression = (rowsAffected == expected)
	case ">":
		bexpression = (rowsAffected > expected)
	case "<":
		bexpression = (rowsAffected < expected)
	case "!=":
		bexpression = (rowsAffected != expected)
	default:
		bexpression = (rowsAffected == expected)
	}

	if !bexpression {
		err = fmt.Errorf("checkResNo - The expected number of updated rows for the operation is %s%d while we got %d", compare, expected, rowsAffected)
		return err
	}
	return nil
}

/* likeManager manages the passed `field` to be used by like clause.
** `check` has exact as default but it can be:
** - `exact`: doesn't put the `%` after or before the field.
** - `starts`: puts `%` after the field
** - `ends`: puts `%` before the field
** - `contains`: puts `%` after and before the field
 */
func likeManager(field string, check string) (managed string) {
	if field == "" {
		managed = "%"
	} else {
		switch check {
		case "exact":
			managed = field
		case "starts":
			managed = field + "%"
		case "contains":
			managed = "%" + field + "%"
		case "ends":
			managed = "%" + field
		default:
			managed = field
		}
	}
	return managed
}

// setFilePath moves the current file and folder when name and authors are updated and changes the references in book and data tables
func setFilePath(bookId int64, legacyTitle string, legacyMainAuth string, newTitle string, newAuthor string, bookFormat string, base string) (err error) {
	//rename the file first as name - author (keep the folder part as legacy)
	legacyFile := fmt.Sprintf("%s%s%s%s%s (%d)%s%s - %s.%s", base, string(os.PathSeparator), legacyMainAuth, string(os.PathSeparator), legacyTitle, bookId, string(os.PathSeparator), legacyTitle, legacyMainAuth, strings.ToLower(bookFormat))
	newFile := fmt.Sprintf("%s%s%s%s%s (%d)%s%s - %s.%s", base, string(os.PathSeparator), legacyMainAuth, string(os.PathSeparator), legacyTitle, bookId, string(os.PathSeparator), newTitle, newAuthor, strings.ToLower(bookFormat))
	err = os.Rename(legacyFile, newFile)
	if err != nil {
		err = fmt.Errorf("setFilePath - Error while renaming the book file %s to %s: %v", legacyFile, newTitle, err)
		return err
	}
	// rename the book title folder
	legacyFolder := fmt.Sprintf("%s%s%s%s%s (%d)", base, string(os.PathSeparator), legacyMainAuth, string(os.PathSeparator), legacyTitle, bookId)
	newFolder := fmt.Sprintf("%s%s%s%s%s (%d)", base, string(os.PathSeparator), legacyMainAuth, string(os.PathSeparator), newTitle, bookId)
	if legacyFolder != newFolder {
		err = os.Rename(legacyFolder, newFolder)
		if err != nil {
			err = fmt.Errorf("setFilePath - Error while renaming the %s folder to %s: %v", legacyFolder, legacyFolder, err)
			return err
		}
	}

	// move to the author book folder if needed
	if newAuthor != legacyMainAuth {
		//legacyAuthFolder := fmt.Sprintf("%s%s%s", base, string(os.PathSeparator), pathElems[0])
		//toMove := fmt.Sprintf("%s%s%s - %s.%s",newFolder,string(os.PathSeparator),newTitle, newAuthor, strings.ToLower(bookToUpdate.Format))
		// create the new author folder
		newAuthFolder := fmt.Sprintf("%s%s%s", base, string(os.PathSeparator), newAuthor)
		newBookFolder := fmt.Sprintf("%s%s%s (%d)", newAuthFolder, string(os.PathSeparator), newTitle, bookId)

		err = utils.CreateFolder(newAuthFolder)
		if err != nil {
			err = fmt.Errorf("setFilePath - Error while creating the new author folder %s for book file %s: %v", newAuthFolder, newTitle, err)
			return err
		}

		/*
			// create the new book folder in the previously created new author folder
			newBookFolder := fmt.Sprintf("%s%s%s (%d)", newAuthFolder, string(os.PathSeparator), newTitle, bookToUpdate.ID)
			err = createFolder(newBookFolder)
			if err != nil {
				err = fmt.Errorf("setFilePath - Error while creating the new author folder %s for book file %s: %v", newBookFolder, newTitle, err)
				return err
			}
		*/

		// move the legacy book folder content
		err = os.Rename(newFolder, newBookFolder)
		if err != nil {
			err = fmt.Errorf("setFilePath - Error while renaming (moving) the %s book folder to %s: %v", newFolder, newAuthFolder, err)
			return err
		}
	}

	// warning
	fmt.Println("[!] Folder name modified. Some link may be broken if the sql fails after the filesystem has been modified")
	// update both book and data tables
	return
}

// valAttrs gets the list of values from the map
func valAttrs(attrs []Attribute) (values []string) {
	for _, attr := range attrs {
		values = append(values, attr.val)
	}
	return values
}
