// main db package file containing the structure and the db initializaiton, common to all the files
package db

import (
	"context"
	"database/sql"
	"sync"
)

// structure to store the book
type Book struct {
	ID         int64    `json:"_id"`
	Title      string   `json:"title"`
	Authors    []string `json:"authors"`
	Tags       []string `json:"tags"`
	Series     []string `json:"series"`
	SeriesVol  float64  `json:"series_vol"`
	Synopsys   string   `json:"synopsys"`
	Languages  []string `json:"language"`
	Publishers []string `json:"publisher"`
	PubDate    string   `json:"pub_date"`
	Path       string   `json:"file_path"`
	Size       int64    `json:"size"`
	Format     string   `json:"format"`
	Shelf      string   `json:"shelf"`
}

// structure to define the filter to use. Id works for both post and get
type QFilter struct {
	Id    int64 `json:"id,omitempty" form:"bookId"`
	Title struct {
		Text   string `json:"text,omitempty"`
		Filter string `json:"filter,omitempty" binding:"omitempty,txtFilterCriteria"`
	} `json:"title,omitempty"`
	Authors struct {
		List   []string `json:"list"`
		Filter string   `json:"filter" binding:"omitempty,listFilterCriteria"`
	} `json:"authors,omitempty"`
	Series struct {
		Text   string `json:"text"`
		Filter string `json:"filter" binding:"omitempty,txtFilterCriteria"`
	} `json:"series,omitempty"`
	Tags struct {
		List   []string `json:"list"`
		Filter string   `json:"filter" binding:"omitempty,listFilterCriteria"`
	} `json:"tags,omitempty"`
}

// define a struct to set how update the series and author basing on data in title
type ParseUpdate struct {
	Series struct {
		SeriesName struct {
			TitleStartNdx int `json:"title_start_ndx"`
			TitleEndNdx   int `json:"title_end_ndx"`
		} `json:"series_name,omitempty"`
		SeriesNo struct {
			TitleStartNdx int `json:"title_start_ndx"`
			TitleEndNdx   int `json:"title_end_ndx"`
		} `json:"series_no,omitempty"`
	} `json:"series_update,omitempty"`
	Authors struct {
		AuthorsName struct {
			TitleStartNdx int    `json:"title_start_ndx"`
			TitleEndNdx   int    `json:"title_end_ndx"`
			Separator     string `json:"separator,omitempty"`
		} `json:"authors_name,omitempty"`
	}
}

// set the DB and the transaction structs
type SQLiteCalWeb struct {
	db  *sql.DB
	dba *sql.DB
}

// transaction
type Tx struct {
	tx  *sql.Tx
	ctx context.Context
}

// replace by map (?)
type Attribute struct {
	key int64
	val string
}

var (
	cwdb *SQLiteCalWeb
	once sync.Once
)

// DB Instance
func GetDB(db *sql.DB, dba *sql.DB) *SQLiteCalWeb {
	cwdb = &SQLiteCalWeb{
		db:  db,
		dba: dba,
	}
	return cwdb
}
