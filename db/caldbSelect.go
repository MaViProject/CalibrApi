// contains all the select-related functions
package db

import (
	utils "calibrapi/utils"
	"fmt"
)

/*** CRUD Methods ***/

/*
// Create enters a new book adding all the passed entries in the right tables
func (r *SQLiteCalWeb) Create(book Book) (*Book, error) {
	res, err := r.db.Exec("INSERT INTO websites(name, url, rank) values(?,?,?)", website.Name, website.URL, website.Rank)
	if err != nil {
			var sqliteErr sqlite3.Error
			if errors.As(err, &sqliteErr) {
					if errors.Is(sqliteErr.ExtendedCode, sqlite3.ErrConstraintUnique) {
							return nil, ErrDuplicate
					}
			}
			return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
			return nil, err
	}
	website.ID = id

	return &website, nil
}
*/

// GetLibrary returns all the books in the library with all their data. It's an interface using GetBooks with empty filters
func (r *SQLiteCalWeb) GetLibrary() (*[]Book, error) {
	filterAll := QFilter{
		Title: struct {
			Text   string "json:\"text,omitempty\""
			Filter string "json:\"filter,omitempty\" binding:\"omitempty,txtFilterCriteria\""
		}{"", ""},
		Authors: struct {
			List   []string "json:\"list\""
			Filter string   "json:\"filter\" binding:\"omitempty,listFilterCriteria\""
		}{nil, ""},
		Series: struct {
			Text   string "json:\"text\""
			Filter string "json:\"filter\" binding:\"omitempty,txtFilterCriteria\""
		}{"", ""},
		Tags: struct {
			List   []string "json:\"list\""
			Filter string   "json:\"filter\" binding:\"omitempty,listFilterCriteria\""
		}{nil, ""},
	}
	return r.GetBooks(&filterAll)
}

// GetBook returns the book matching with the passed ID. It's an interface using GetBooks with a single ID filter
func (r *SQLiteCalWeb) GetBook(bookId int64) (*[]Book, error) {
	filterId := QFilter{
		Id: bookId,
	}
	return r.GetBooks(&filterId)
}

// GetBooks returns all the books matching the criteria in the filter structure
func (r *SQLiteCalWeb) GetBooks(filters *QFilter) (*[]Book, error) {
	// instantiate the app DB to get the shelf
	/*
		dbapp, err := sql.Open("sqlite3", appDB)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while initializing the app DB: %v", err)
			return nil, err
		}
		defer dbapp.Close()
		app := GetDB(dbapp)
	*/
	app := GetDB(r.dba, nil)

	// set the Title filter to be used in the query
	fmt.Println("Filtro ID:", filters.Id)
	title := likeManager(filters.Title.Text, filters.Title.Filter)
	var id string
	if filters.Id != 0 {
		id = likeManager(fmt.Sprintf("%d", filters.Id), "exact")
	} else {
		id = "%"
	}

	fmt.Println("Applying filter:")
	fmt.Println("\tID:", id)
	fmt.Println("\tTitle:", title)
	fmt.Println("\tAuthor:", filters.Authors)
	fmt.Println("\tSeries:", filters.Series)
	fmt.Println("\tTags:", filters.Tags)

	// get all books matching the criteria from the book table
	getBooksQry := "SELECT id, title as Title, series_index as SeriesVol, pubdate as PubDate, path as Path FROM books WHERE title like ? AND id like ?"
	rows, err := r.db.Query(getBooksQry, title, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// instantiate the array of books as library. It will contain all the data
	var library []Book
	// for each book (tupla) returned by the query maps the value and get additional book info from other query
	for rows.Next() {
		var bk Book
		if err := rows.Scan(&bk.ID, &bk.Title, &bk.SeriesVol, &bk.PubDate, &bk.Path); err != nil {
			err = fmt.Errorf("GetBooks - Error while matching DB: %v", err)
			return nil, err
		}

		// get the authors
		qry := "SELECT a.id, a.name as Author FROM authors a JOIN books_authors_link l ON (a.id = l.author) WHERE l.book = ?"
		auths, err := r.getBookAttrs(qry, bk.ID)
		//auths, err := r.getBookAttrs("", "a.name", "authors a", "books_authors_link l", "l.id", "l.author", "l.book", bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting authors attributes: %v", err)
			return nil, err
		}
		aus := valAttrs(auths)
		bk.Authors = append(bk.Authors, aus...)

		// check if the author is in the filter
		// check if the book authors matches all the ones in the author (filter contained in the book author list)
		if filters.Authors.List != nil && len(filters.Authors.List) > 0 {
			// filter on array basing on the filter type (continue skip this for iteration)
			if filters.Authors.Filter == "exact" {
				if !utils.SameContentSlice(filters.Authors.List, bk.Authors) {
					continue
				}
			} else if filters.Authors.Filter == "contains" {
				// save resources by skipping the book (don't reach the append to the library) as its tags don't match the filter
				if !utils.ContainedSlice(filters.Authors.List, bk.Authors) {
					continue
				}
			}
		}

		// get the series even if it by structure may belong to more than once, it is only 1 per book
		qry = "SELECT s.id, s.name as Serie FROM series s JOIN books_series_link l ON (s.id = l.series) WHERE l.book = ?"
		series, err := r.getBookAttrs(qry, bk.ID)
		//series, err := r.getBookAttrs("", "name", "series", "books_series_link", "id", "series", "book", bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting series attributes: %v", err)
			return nil, err
		}
		ss := valAttrs(series)
		bk.Series = append(bk.Series, ss...)
		if filters.Series.Text != "" {
			// if search by series and book has no seires then skip
			if len(bk.Series) == 0 {
				continue
			} else if !utils.CheckStringMatch(filters.Series.Text, bk.Series[0], filters.Series.Filter) {
				// as it's just 1 series per book check if the filter text matches with the first array occurrence in book series
				continue
			}
		}

		// get the tags
		qry = "SELECT t.id, t.name as Tag FROM tags t JOIN books_tags_link l ON (t.id = l.tag) WHERE l.book = ?"
		tags, err := r.getBookAttrs(qry, bk.ID)
		//tags, err := r.getBookAttrs("", "name", "tags", "books_tags_link", "id", "tag", "book", bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting tags attributes: %v", err)
			return nil, err
		}
		tgs := valAttrs(tags)
		bk.Tags = append(bk.Tags, tgs...)
		// check if the book tags matches all the ones in the filter (filter contained in the book tag list)
		if filters.Tags.List != nil && len(filters.Tags.List) > 0 {
			// filter on array basing on the filter type (continue skip this for iteration)
			if filters.Tags.Filter == "exact" {
				if !utils.SameContentSlice(filters.Tags.List, bk.Tags) {
					continue
				}
			} else if filters.Tags.Filter == "contains" {
				if !utils.ContainedSlice(filters.Tags.List, bk.Tags) {
					continue
				}
			}
		}
		// get the languages
		//qry = "SELECT l.id, l.lang_code as Language FROM books_languages_link l WHERE l.book = ?" //old returning id not code
		qry = "SELECT l.id, ln.lang_code as Language FROM books_languages_link l JOIN languages ln ON (l.lang_code = ln.id) WHERE l.book = ?"
		langs, err := r.getBookAttrs(qry, bk.ID)
		//langs, err := r.getBookAttrs("", "lang_code", "languages", "books_languages_link", "id", "lang_code", "book", bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting languages attributes: %v", err)
			return nil, err
		}
		lgs := valAttrs(langs)
		bk.Languages = append(bk.Languages, lgs...)

		// get the publisher
		qry = "SELECT p.id, p.name as Publisher FROM publishers p JOIN books_publishers_link l ON (p.id = l.publisher) WHERE l.book = ?"
		publishers, err := r.getBookAttrs(qry, bk.ID)
		//publishers, err := r.getBookAttrs("", "name", "publishers", "books_publishers_link", "id", "publisher", "book", bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting publisher attributes: %v", err)
			return nil, err
		}
		pbs := valAttrs(publishers)
		bk.Publishers = append(bk.Publishers, pbs...)

		// get the synopsys (single attribute per book)
		var syn string
		qry = "SELECT c.id, c.text as Synopsys FROM comments c WHERE c.book = ?"
		synArr, err := r.getBookAttrs(qry, bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting synopsys attribute: %v", err)
			return nil, err
		}
		if synArr != nil {
			syn = synArr[0].val
		}
		bk.Synopsys = syn

		// get the book size and format (2 different attributes but only 1 instance). Directly add to bk
		qry = "SELECT d.format as Format, d.uncompressed_size as Size FROM data d WHERE d.book = ?"
		rows, err := r.db.Query(qry, bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting size and format attributes: %v", err)
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			if err := rows.Scan(&bk.Format, &bk.Size); err != nil {
				return nil, err
			}
		}

		//get the shelf for the book from the other DB
		var shelf string
		qry = "SELECT s.id, s.name as Shelf FROM shelf s JOIN book_shelf_link l ON (s.id = l.shelf) WHERE l.book_id = ?"
		shlfArr, err := app.getBookAttrs(qry, bk.ID)
		//shlfArr, err := app.getBookAttrs("", "name", "shelf", "book_shelf_link", "id", "shelf", "book_id", bk.ID)
		if err != nil {
			err = fmt.Errorf("GetBooks - Error while getting shelf attribute: %v", err)
			return nil, err
		}
		if shlfArr != nil {
			shelf = shlfArr[0].val
		}
		bk.Shelf = shelf

		// add the assembled book info to the library
		library = append(library, bk)
	}
	return &library, nil
}
