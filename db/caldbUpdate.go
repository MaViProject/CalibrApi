package db

import (
	"context"
	"fmt"
	"strconv"
	"strings"
)

// Many attributes are treated as a list of values. Set all the peculiarities in the struct passed to the function to manage the updates
type AttrListParams struct {
	Qry         string
	AttrList    []string
	BookCol     string
	LnkTbl      string
	LnkCol      string
	CatTbl      string
	CatCol      string
	TriggerName string
}

// Update gets the books matching given parameters in `filters` basing on `updates`. Use the `filePath` to move the file according with Calibre logic. If a catalogue entry doesn't exist it is created. Multiple actions needed so transaction is used.
func (r *SQLiteCalWeb) UpdateBooks(ctx context.Context, filters *QFilter, parseUpd *ParseUpdate, updates *Book, filePath string, ltitplc string, dbAppPath string) (*[]Book, error) {
	// Create a helper function for preparing failure results.
	fail := func(err error) error {
		return fmt.Errorf("UpdateBooks: %v", err)
	}

	// use the filter to get the list of books to update
	booksToUpdate, err := r.GetBooks(filters)
	if err != nil {
		err = fmt.Errorf("Error while getting books to update list: %v", err)
		return nil, fail(err)
	}

	// store the list of updated books to be returned
	var updLibrary []Book
	// for each book in the booklist to update do the following
	for _, bookToUpdate := range *booksToUpdate {
		// start the transaction
		trx, err := r.db.BeginTx(ctx, nil)
		if err != nil {
			return nil, fail(err)
		}
		// Defer a rollback in case anything fails.
		defer trx.Rollback()
		tx := Tx{
			tx:  trx,
			ctx: ctx,
		}

		// store the legacy parameters needed to change the file path (need to clone)
		legacyFirstAuth := strings.Clone(bookToUpdate.Authors[0])
		legacyTitle := strings.Clone(bookToUpdate.Title)
		fmt.Println("legacyFirstAuth", legacyFirstAuth)

		// update the attribute tables first then the main book one

		// update the series table (unique per book but with the same structure of attribute list) eventually checking if by parsing title
		if (updates.Series != nil && len(updates.Series) > 0) || parseUpd.Series.SeriesName.TitleStartNdx > 0 {
			var seriesName []string
			if updates.Series != nil && len(updates.Series) > 0 {
				seriesName = updates.Series
			} else {
				seriesName = append(seriesName, legacyTitle[parseUpd.Series.SeriesName.TitleStartNdx:parseUpd.Series.SeriesName.TitleEndNdx])
			}
			seriesAttrs := AttrListParams{
				Qry:         "SELECT s.id, s.name as Series FROM series s JOIN books_series_link l ON (s.id = l.series) WHERE l.book = ?",
				BookCol:     "book",
				LnkTbl:      "books_series_link",
				LnkCol:      "series",
				CatTbl:      "series",
				CatCol:      "name",
				TriggerName: "series_insert_trg",
				AttrList:    seriesName,
			}
			err = tx.updAttributeList(bookToUpdate.ID, &seriesAttrs, r)
			//err = tx.updateSeries(bookToUpdate.ID, updates.Series, r)
			if err != nil {
				err = fmt.Errorf("Series update: %v", err)
				return nil, fail(err)
			}
		}

		// updates the authors table (eventually checking if by parsing title)
		// set the main author to pass to update data and book tables
		mainAuthor := bookToUpdate.Authors[0]
		if (updates.Authors != nil && len(updates.Authors) > 0) || (parseUpd.Authors.AuthorsName.TitleStartNdx > 0) { // direct update
			var autlist []string
			if updates.Authors != nil && len(updates.Authors) > 0 {
				autlist = updates.Authors
				mainAuthor = updates.Authors[0]
			} else {
				// parse the legacy title portion definrd by indexes and split by separator
				autlist = strings.Split(legacyTitle[parseUpd.Authors.AuthorsName.TitleStartNdx-1:parseUpd.Authors.AuthorsName.TitleEndNdx], parseUpd.Authors.AuthorsName.Separator)
				mainAuthor = autlist[0]
			}

			authorsAttrs := AttrListParams{
				Qry:         "SELECT a.id, a.name as Author FROM authors a JOIN books_authors_link l ON (a.id = l.author) WHERE l.book = ?",
				BookCol:     "book",
				LnkTbl:      "books_authors_link",
				LnkCol:      "author",
				CatTbl:      "authors",
				CatCol:      "name",
				TriggerName: "",
				AttrList:    autlist,
			}
			err = tx.updAttributeList(bookToUpdate.ID, &authorsAttrs, r)
			if err != nil {
				err = fmt.Errorf("Authors update: %v", err)
				return nil, fail(err)
			}
		}

		// updates the tags
		if updates.Tags != nil && len(updates.Tags) > 0 {
			tagsAttrs := AttrListParams{
				Qry:         "SELECT t.id, t.name as Tag FROM tags t JOIN books_tags_link l ON (t.id = l.tag) WHERE l.book = ?",
				BookCol:     "book",
				LnkTbl:      "books_tags_link",
				LnkCol:      "tag",
				CatTbl:      "tags",
				CatCol:      "name",
				TriggerName: "",
				AttrList:    updates.Tags,
			}
			//err = tx.updateTags(bookToUpdate.ID, updates.Tags, r)
			err = tx.updAttributeList(bookToUpdate.ID, &tagsAttrs, r)
			if err != nil {
				err = fmt.Errorf("Tags update: %v", err)
				return nil, fail(err)
			}
		}

		// updates the languages
		if updates.Languages != nil && len(updates.Languages) > 0 {
			langAttrs := AttrListParams{
				Qry:         "SELECT l.id,l.lang_code as Language FROM languages l JOIN books_languages_link lk WHERE lk.book= ?",
				BookCol:     "book",
				LnkTbl:      "books_languages_link",
				LnkCol:      "lang_code",
				CatTbl:      "languages",
				CatCol:      "lang_code",
				TriggerName: "",
				AttrList:    updates.Languages,
			}
			err = tx.updAttributeList(bookToUpdate.ID, &langAttrs, r)
			if err != nil {
				err = fmt.Errorf("Languages update: %v", err)
				return nil, fail(err)
			}
		}

		// update publisher (unique per book but with the same structure of list)
		if updates.Publishers != nil && len(updates.Publishers) > 0 {
			publAttrs := AttrListParams{
				Qry:         "SELECT p.id, p.name as Publisher FROM publishers p JOIN books_publishers_link l ON (p.id = l.publisher) WHERE l.book = ?",
				BookCol:     "book",
				LnkTbl:      "books_publishers_link",
				LnkCol:      "publisher",
				CatTbl:      "publishers",
				CatCol:      "name",
				TriggerName: "",
				AttrList:    updates.Publishers,
			}
			err = tx.updAttributeList(bookToUpdate.ID, &publAttrs, r)
			if err != nil {
				err = fmt.Errorf("Publishers update: %v", err)
				return nil, fail(err)
			}
		}

		// update the book synopsys, not a linked table so update directly with its own logic
		if updates.Synopsys != "" && len(updates.Synopsys) > 0 {
			err = tx.updateSynopsys(bookToUpdate.ID, updates.Synopsys)
			if err != nil {
				err = fmt.Errorf("Synopsys update: %v", err)
				return nil, fail(err)
			}
		}

		// set the title to pass to update data and book tables, considering the title placeholder to modify it instead of replacing it.
		var mainTitle string
		if updates.Title != "" {
			// if updates by modifying the legacy title, replace the placeholder in the string with the legacy title value. If not return just the string
			mainTitle = strings.Replace(updates.Title, ltitplc, legacyTitle, -1)
		} else {
			mainTitle = bookToUpdate.Title
		}

		// update the data table basing on title and first author if any has changed, not a linked table so update directly with its own logic
		//if updates.Title != bookToUpdate.Title || updates.Authors[0] != bookToUpdate.Authors[0] {
		err = tx.updateDataTable(bookToUpdate.ID, mainTitle, mainAuthor)
		if err != nil {
			err = fmt.Errorf("Data table update: %v", err)
			return nil, fail(err)
		}
		//}

		// update the main book table. To make the statement "idempotent" replace the values for the books to update with the ones to update but save the legacy title to allow file management.
		// title
		if updates.Title != "" && len(updates.Title) > 0 {
			bookToUpdate.Title = updates.Title
		}
		// series vol nr from updates or from legacy title parsing
		if updates.SeriesVol > 0 || parseUpd.Series.SeriesNo.TitleStartNdx > 0 {
			if updates.SeriesVol > 0 {
				bookToUpdate.SeriesVol = float64(updates.SeriesVol)
			} else {
				volNr, err := strconv.ParseFloat(legacyTitle[parseUpd.Series.SeriesNo.TitleStartNdx-1:parseUpd.Series.SeriesNo.TitleEndNdx], 64)
				if err != nil {
					err = fmt.Errorf("converting the volume number get by legacy title substring to float64 for %s: %v", bookToUpdate.Title, err)
					return nil, fail(err)
				}
				bookToUpdate.SeriesVol = volNr
			}
		}
		fmt.Println("Data pubblicazione pre", bookToUpdate.PubDate)
		// date as generic day and month (Jan the 1st) of the given year at geenric 00 time as DB field is a timestamp
		if updates.PubDate != "" && len(updates.PubDate) > 0 {
			fmt.Println("Modifico data pub in", updates.PubDate)
			bookToUpdate.PubDate = fmt.Sprintf("%s-01-01 00:00:00.000000", updates.PubDate)
		} else {
			//reformat the retrieved date
			yr := bookToUpdate.PubDate[0:4]
			bookToUpdate.PubDate = fmt.Sprintf("%s-01-01 00:00:00.000000", yr)
		}

		fmt.Println("Data pub post", bookToUpdate.PubDate)

		// call the function to update the book
		err = tx.updateMainBook(bookToUpdate.ID, mainTitle, bookToUpdate.SeriesVol, bookToUpdate.PubDate, mainAuthor)
		if err != nil {
			err = fmt.Errorf("main book table update: %v", err)
			return nil, fail(err)
		}

		//update the shelf in a separate db (if needed)
		if updates.Shelf != "" && len(updates.Shelf) > 0 {
			err = r.updateShelf(ctx, dbAppPath, bookToUpdate.ID, updates.Shelf)
			if err != nil {
				err = fmt.Errorf("shelf update: %v", err)
				return nil, fail(err)
			}
		}

		// if everything was fine commit
		if err = tx.tx.Commit(); err != nil {
			return nil, fail(err)
		}
		// query the book by id to return the new data as bookToUpdate hasn't been updated but for the main book table
		updated, err := r.GetBook(bookToUpdate.ID)
		if err != nil {
			err = fmt.Errorf("error retrieving the updated book data: %v", err)
			return nil, fail(err)
		}
		updLibrary = append(updLibrary, *updated...)

		//then change the file paths (file system ops cannot be rolled back once failed)
		fmt.Println("[i] Check the data to rename the file:")
		fmt.Println("\t[i] Book Id:", bookToUpdate.ID)
		fmt.Println("\t[i] Legacy Title:", legacyTitle)
		fmt.Println("\t[i] New Title:", mainTitle)
		fmt.Println("\t[i] Legacy MainAuthor:", legacyFirstAuth)
		fmt.Println("\t[i] New Main Author:", mainAuthor)
		fmt.Println("\t[i] Book Format:", bookToUpdate.Format)
		err = setFilePath(bookToUpdate.ID, legacyTitle, legacyFirstAuth, mainTitle, mainAuthor, bookToUpdate.Format, filePath)
		if err != nil {
			err = fmt.Errorf("bookID %d: %v", bookToUpdate.ID, err)
			return nil, fail(err)
		}
		fmt.Println("Updated is:", updated)

	} // for ends here (move to the next book)

	return &updLibrary, nil
}

//*** SUPPORT UPDATE FUNCTIONS ***

// updateSynopsys receives the bookId to identify what to update in the comments table. It has a different logic from the list attribute updates
func (tx *Tx) updateSynopsys(bookId int64, newSynopsys string) (err error) {
	// check if there is already a synopsys so it's an update or a new insert for the book
	qry := "SELECT id FROM comments c WHERE c.book = ?"
	synRes, err := tx.tx.QueryContext(tx.ctx, qry, bookId)
	if err != nil {
		err = fmt.Errorf("updateSynopsys - Error while querying the synopsys for book %d: %v", bookId, err)
		return err
	}
	defer synRes.Close()

	//iterates through the matching instances (just 1 for the synopsys)
	var synId int64
	for synRes.Next() {
		if err := synRes.Scan(&synId); err != nil {
			err = fmt.Errorf("updateSynopsys - Error scanning query results for synopsys for book %d: %v", bookId, err)
			return err
		}
	}
	var synQry string
	if synId > 0 {
		synQry = "UPDATE comments SET text = ? WHERE book = ?"
	} else {
		synQry = "INSERT INTO comments(text, book) VALUES(?, ?)"
	}
	res, err := tx.tx.ExecContext(tx.ctx, synQry, newSynopsys, bookId)
	if err != nil {
		err = fmt.Errorf("updateSynopsys - Error while entering synopsys %s for book %d: %v", newSynopsys, bookId, err)
		return err
	}
	err = checkResNo(res, "==", 1)
	if err != nil {
		err = fmt.Errorf("updateSynopsys - Error for book %d: %v", bookId, err)
		return err
	}
	return nil
}

// update data table setting (the path needed to open the book)
func (tx *Tx) updateDataTable(bookId int64, newTitle string, newAuthor string) (err error) {
	dataName := fmt.Sprintf("%s - %s", newTitle, newAuthor)
	updDataQry := "UPDATE data SET name = ? WHERE book = ?"
	res, err := tx.tx.ExecContext(tx.ctx, updDataQry, dataName, bookId)
	if err != nil {
		err = fmt.Errorf("updateDataTable - Error while updating the data table with the new name %s for book %d: %v", dataName, bookId, err)
		return err
	}
	err = checkResNo(res, "==", 1)
	if err != nil {
		err = fmt.Errorf("updateDataTable - Error for book %d: %v", bookId, err)
		return err
	}
	return nil
}

func (tx *Tx) updateMainBook(bookId int64, title string, seriesVol float64, pubDate string, mainAuthor string) (err error) {
	// set the new path in the main book table (it doesn't change if the data are the same)
	newPath := fmt.Sprintf("%s/%s (%d)", mainAuthor, title, bookId)
	// define the trigger to remove to update the main book table
	actingTrigger := "books_update_trg"
	// get the sql statement to create the trigger
	triggerQry, err := tx.getTrigger(actingTrigger)
	if err != nil {
		err = fmt.Errorf("updateMainBook - bookID %d: %v", bookId, err)
		return err
	}
	// drop the existing trigger as it creates problems with the update
	err = tx.dropTrigger(actingTrigger)
	if err != nil {
		err = fmt.Errorf("updateMainBook - bookID %d: %v", bookId, err)
		return err
	}

	// update the main book table in the DB
	updBookQry := "UPDATE books SET title = ?, series_index = ?, pubdate = ?, path = ? WHERE id = ?"
	res, err := tx.tx.ExecContext(tx.ctx, updBookQry, title, seriesVol, pubDate, newPath, bookId)
	if err != nil {
		err = fmt.Errorf("UpdateBooks - Error while executing update query on bookID %v: %v", bookId, err)
		return err
	}
	err = checkResNo(res, `==`, 1)
	if err != nil {
		err = fmt.Errorf("UpdateBooks - bookID %v: %v", bookId, err)
		return err
	}

	// re-create books_update_trg trigger
	err = tx.createTrigger(triggerQry)
	if err != nil {
		err = fmt.Errorf("UpdateBooks - bookID %d: %v", bookId, err)
		return err
	}
	return nil
}

/* TO REWRITE AS IT USES r POINTING TO METADATA DB and recreate the dbapp already in server
// updateShelf updates the shelf in the `app.db` database (added by CalibreWeb to avoid editing the original calibre db). Could be used as update attr list but it's managed by it's own transaction defined here.
func (tx *Tx) updateShelf(appdbPath string, bookId int64, newShelf string, r *SQLiteCalWeb) (err error) {
	// instantiate the app DB to get the shelf
	dbapp, err := sql.Open("sqlite3", appdbPath)
	if err != nil {
		err = fmt.Errorf("updateShelf - Error while initializing the app DB: %v", err)
		return err
	}
	defer dbapp.Close()
	app := GetDB(dbapp, nil)
	// start the transaction
	trxa, err := app.db.BeginTx(tx.ctx, nil)
	if err != nil {
		return err
	}
	// Defer a rollback in case anything fails.
	defer trxa.Rollback()
	txa := Tx{
		tx:  trxa,
		ctx: tx.ctx,
	}
	var confShelfArray []string
	confShelfArray = append(confShelfArray, newShelf)
	shelfAttrs := AttrListParams{
		Qry:         "SELECT s.id, s.name as Shelf FROM shelf s JOIN book_shelf_link l ON (s.id = l.shelf) WHERE l.book_id = ?",
		BookCol:     "book_id",
		LnkTbl:      "book_shelf_link",
		LnkCol:      "shelf",
		CatTbl:      "shelf",
		CatCol:      "name",
		TriggerName: "",
		AttrList:    confShelfArray,
	}
	err = txa.updAttributeList(bookId, &shelfAttrs, r)
	if err != nil {
		err = fmt.Errorf("updateShelf: %v", err)
		return err
	}
	// Commit the transactions
	if err = txa.tx.Commit(); err != nil {
		return err
	}
	return nil
}
*/

func (r *SQLiteCalWeb) updateShelf(ctx context.Context, appdbPath string, bookId int64, newShelf string) (err error) {
	// need to use dba as it was db (temp solution before refactoring)
	app := GetDB(r.dba, nil)
	// start the transaction
	trxa, err := app.db.BeginTx(ctx, nil)

	//trxa, err := r.dba.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	// Defer a rollback in case anything fails.
	defer trxa.Rollback()
	txa := Tx{
		tx:  trxa,
		ctx: ctx,
	}
	var confShelfArray []string
	confShelfArray = append(confShelfArray, newShelf)
	shelfAttrs := AttrListParams{
		Qry:         "SELECT s.id, s.name as Shelf FROM shelf s JOIN book_shelf_link l ON (s.id = l.shelf) WHERE l.book_id = ?",
		BookCol:     "book_id",
		LnkTbl:      "book_shelf_link",
		LnkCol:      "shelf",
		CatTbl:      "shelf",
		CatCol:      "name",
		TriggerName: "",
		AttrList:    confShelfArray,
	}
	// cambiare i metodi: non puntano a *SQLiteCalWeb ma a *sql.DB altrimenti non posso usare lo stesso metodo per un db diverso (e.g. dbapp)
	err = txa.updAttributeList(bookId, &shelfAttrs, app)
	if err != nil {
		err = fmt.Errorf("updateShelf: %v", err)
		return err
	}
	// Commit the transactions
	if err = txa.tx.Commit(); err != nil {
		return err
	}
	return nil
}
