package main

import (
	api "calibrapi/api"
	db "calibrapi/db"
	"database/sql"
	"fmt"
	"os"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
)

// Store the configuration info
type Config struct {
	DBMeta     string `mapstructure:"BOOK_DB_PATH"`
	DBApp      string `mapstructure:"APP_DB_PATH"`
	ServerIP   string `mapstructure:"SERVER_ADDR"`
	ServerPort string `mapstructure:"SERVER_PORT"`
	TitlePlc   string `mapstructure:"LEGACY_TITLE_PLACEHOLDER"`
	LogLevel   string `mapstructure:"LOG_LEVEL"`
	LogSizeMB  string `mapstructure:"LOG_SIZE_MB"`
	LogRotate  string `mapstructure:"LOG_ROTATE_DAYS"`
}

var (
	// info var
	__app__     string = "CalibrApi"
	__version__ string = "0.1.0"
	__author__  string = "m3o"

	// configuration variables
	conf         Config   // configuration storage
	confPath     = "."    //config file is in the root
	confFile     = "conf" //config file name
	confFileType = "env"  //config file extension
)

// Use Viper to load the configuration in the passed struct
func (cfg *Config) loadConfig() (err error) {
	viper.AddConfigPath(confPath)
	viper.SetConfigName(confFile)
	viper.SetConfigType(confFileType)

	// overrides from environment variables if passed when the system is started
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(&conf)
	return err
}

// welcome message and load config
func init() {
	//print app data
	welcome := fmt.Sprintf("%s v.%s \nby %s", __app__, __version__, __author__)
	fmt.Println(welcome)

	//load base config
	err := conf.loadConfig()
	if err != nil {
		fmt.Print(fmt.Sprintf("Error while loading the configuration: %v", err))
		os.Exit(1) //gently quits
	}

}

// main
func main() {

	// get the DB and instantiate a connector
	fmt.Println("Loaded DB:", conf.DBMeta)
	dbmeta, err := sql.Open("sqlite3", conf.DBMeta)
	if err != nil {
		err = fmt.Errorf("Error while initializing the library DB: %v", err)
		fmt.Println("[-]", err)
		os.Exit(1) // gently quit
	}
	defer dbmeta.Close()

	// instantiate the app DB to get the shelf
	dbapp, err := sql.Open("sqlite3", conf.DBApp)
	if err != nil {
		err = fmt.Errorf("Error while initializing the application DB: %v", err)
		fmt.Println("[-]", err)
		os.Exit(1) // gently quit
	}
	defer dbapp.Close()

	// get the base path from the DB location
	basePath := filepath.Dir(conf.DBMeta)

	library := db.GetDB(dbmeta, dbapp)
	server := api.NewServer(library, basePath, conf.TitlePlc, conf.DBApp)

	//start the server
	srvaddr := fmt.Sprintf("%s:%s", conf.ServerIP, conf.ServerPort)
	err = server.Start(srvaddr)
	if err != nil {
		err = fmt.Errorf("Error while starting the server API: %v", err)
		fmt.Println("[-]", err)
		os.Exit(1) // gently quit
	}
}
